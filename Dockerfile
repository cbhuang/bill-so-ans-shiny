FROM frolvlad/alpine-miniconda3

# mirror project files
RUN mkdir -p /opt/bill-so-ans-shiny
WORKDIR /opt/bill-so-ans-shiny
COPY . .

# restore environment
RUN conda env create --name bill-so-ans-shiny -f conda-env-r.yml && \
    conda env create --name bill-so-ans -f conda-env-python.yml && \
    conda clean --all

# run
EXPOSE 3838
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "bill-so-ans-shiny", "Rscript", "runApp.R"]
