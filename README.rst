=======================================================
Analysis of Bill's Stack Overflow Answers (R Shiny)
=======================================================

Introduction
=====================

This is the R Shiny version of `Analysis of Bill's Stack Overflow Answers <https://gitlab.com/cbhuang/bill-so-ans>`_.
Please refer to the above link for introduction and system prerequisites.


Test Procedure
=====================

Assume a ``bash``-like console interface.


1. Download and restore environment
----------------------------------------------------
.. code-block:: bash

    $ git clone bill-so-ans-shiny
    $ cd bill-so-ans-shiny
    $ conda env create --name bill-so-ans-shiny --file conda-env-r.yml
    $ conda env create --name bill-so-ans --file conda-env-python.yml
    $ conda activate bill-so-ans-shiny

Note that one must also restore Python environment for the analytical module.


2A. Build and run docker image locally
----------------------------------------------------

To be used when CD/CI tools were not set up.

.. code-block:: bash

    # build docker image
    (bill-so-ans-shiny) $ docker build -t bill-so-ans-shiny .

    # create container and run
    (bill-so-ans-shiny) $ docker-compose up

And then open `<http://localhost:3838>`_ in the browser.

2B. Direct execution
----------------------------

Not recommended for production use.

.. code-block:: bash

    (bill-so-ans-shiny) $ Rscript runApp.py

    # open http://localhost:3838 in the browser
